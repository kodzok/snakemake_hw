import pandas as pd
from sklearn.preprocessing import MinMaxScaler

from src.utils.arg_parser import parse_arguments


def scale(data_path: str, output_path: str) -> None:
    df = pd.read_csv(data_path)
    scaler = MinMaxScaler()

    x_scaled = scaler.fit_transform(df.drop(columns=["target"]))
    iris_scaled_df = pd.DataFrame(
        data=x_scaled, columns=df.drop(columns=["target"]).columns
    )
    iris_scaled_df["target"] = df["target"]

    iris_scaled_df.to_csv(output_path, index=False)


if __name__ == "__main__":
    args = parse_arguments()

    scale(args.data_path, args.output_path)
