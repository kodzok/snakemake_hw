import pandas as pd
from sklearn.datasets import load_iris


def load_data() -> None:
    iris = load_iris()
    df = pd.DataFrame(iris.data, columns=iris.feature_names)
    df["target"] = iris.target
    df.to_csv("data/raw/iris.csv", index=False)


if __name__ == "__main__":
    load_data()
