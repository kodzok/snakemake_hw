import argparse


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("data_path", type=str, help="path to preprocessed data")
    parser.add_argument("output_path", type=str, help="path to output file")
    return parser.parse_args()
