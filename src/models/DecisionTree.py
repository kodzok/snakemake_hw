import pickle

import pandas as pd
from sklearn.tree import DecisionTreeClassifier

from src.utils.arg_parser import parse_arguments


def train_model(data_path: str, output_path: str) -> None:
    df = pd.read_csv(data_path)
    x_train = df.drop(columns=["target"]).values
    y_train = df["target"].values

    logreg = DecisionTreeClassifier()
    logreg.fit(x_train, y_train)

    pickle.dump(logreg, open(output_path, "wb"))


if __name__ == "__main__":
    args = parse_arguments()

    train_model(args.data_path, args.output_path)
