# Repo for ODS Snakemake HW

## Description
This repository contains a machine learning pipeline for training models
on the Iris dataset. The pipeline consists of several steps including data
generation, data preprocessing, and model training. Each step is executed
sequentially, and the final trained models are saved for further use.

## Pipeline Steps:
1. **Generate Data:**
   - Description: This step generates raw data for the model training
   process.
   - Input: None
   - Output: `data/raw/iris.csv`
2. **Preprocess Data:**
   - Description: This step preprocesses the raw data for model training
   using various preprocessing methods.
   - Input: `data/raw/iris.csv`
   - Output: `data/preprocessed/{preprocess_method}/preprocessed.csv`
3. **Train Model:**
   - Description: This step trains the machine learning model using the
   preprocessed data.
   - Input: `data/preprocessed/{preprocess_method}/preprocessed.csv`
   - Output: `models/{train_method}/{preprocess_method}/model.pkl`

## Directed Acyclic Graph
The Directed Acyclic Graph (DAG) below illustrates the dependencies
between the steps of the pipeline. Each node represents a step, and the
arrows indicate the flow of data between them.


![DAG](images/dag.svg)
