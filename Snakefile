import multiprocessing

num_threads = multiprocessing.cpu_count()


configfile: "config.yaml"


rule all:
    input:
        expand(
            "models/{train_method}/{preprocess_method}/model.pkl",
            train_method=config["train_methods"],
            preprocess_method=config["preprocess_methods"],
        ),


rule generate_data:
    output:
        "data/raw/iris.csv",
    shell:
        """
        mkdir -p data/raw
        python src/data/load_iris.py
        """


rule preprocess:
    input:
        "data/raw/iris.csv",
    output:
        "data/preprocessed/{preprocess_method}/preprocessed.csv",
    threads: min(num_threads // 2, 1)
    shell:
        """
        mkdir -p data/preprocessed/{wildcards.preprocess_method}
        export PYTHONPATH=./
        python src/data/{wildcards.preprocess_method}.py {input} {output}
        """


rule train_model:
    input:
        "data/preprocessed/{preprocess_method}/preprocessed.csv",
    output:
        "models/{train_method}/{preprocess_method}/model.pkl",
    threads: min(num_threads // 2, 1)
    shell:
        """
        mkdir -p models/{wildcards.train_method}/{wildcards.preprocess_method}
        export PYTHONPATH=./
        python src/models/{wildcards.train_method}.py {input} {output}
        """
